package com.example.bookapp

import android.app.Application

class BookApp : Application() {

    override fun onCreate() {
        super.onCreate()
        app = this
    }

    companion object {

        private var app: BookApp? = null

        @Synchronized
        fun get(): BookApp {
            if (app == null) {
                app = BookApp()
            }
            return app!!
        }
    }


}