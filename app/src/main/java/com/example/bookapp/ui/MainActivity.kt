package com.example.bookapp.ui

import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.transition.Slide
import com.example.bookapp.R
import com.example.bookapp.databinding.ActivityMainBinding
import com.example.bookapp.hideKeyboard
import com.example.bookapp.model.Book
import com.example.bookapp.toJson
import com.example.bookapp.ui.fragments.FragmentAddBook
import com.example.bookapp.ui.fragments.FragmentBookDetail
import com.example.bookapp.ui.fragments.FragmentListBooks


class MainActivity : AppCompatActivity() {

    private var openingFragment = false

    private var builder: AlertDialog.Builder? = null

    private var dialog: AlertDialog? = null

    private lateinit var binding: ActivityMainBinding

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        toolbar = binding.mainToolbar
        setSupportActionBar(toolbar)

        mainFragment()
    }

    private fun mainFragment() {
        val fragment = FragmentListBooks()

        val transaction = supportFragmentManager.beginTransaction()

        transaction.replace(R.id.main_frame, fragment, FragmentListBooks.TAG)
        transaction.commit()
    }

    private fun configureToolbar(showBackButton: Boolean) {

        supportActionBar?.let {

            it.setDisplayHomeAsUpEnabled(showBackButton)
            it.setDisplayShowHomeEnabled(showBackButton)

            toolbar.setNavigationOnClickListener {
                onBackPressed()
            }
        }
    }

    fun fragmentBookDetail(book: Book) {

        val tag = FragmentBookDetail.TAG

        val bundle = Bundle()
        bundle.putString(FragmentBookDetail.BOOK, book.toJson())

        val fragment = FragmentBookDetail()
        fragment.arguments = bundle

        addFragment(fragment, tag)

        configureToolbar(true)
    }

    fun fragmentAddBook() {

        val tag = FragmentAddBook.TAG

        val fragment = FragmentAddBook()

        addFragment(fragment, tag)

        configureToolbar(true)
    }

    private fun addFragment(fragment: Fragment, tag: String) {

        if (this.openingFragment) return

        this.openingFragment = true

        fragment.enterTransition = Slide(Gravity.END)
        fragment.exitTransition = Slide(Gravity.START)

        val manager = supportFragmentManager

        manager.findFragmentByTag(tag)?.let {
            manager.beginTransaction().remove(fragment).commit()
        }

        val transaction = manager.beginTransaction()
        transaction.add(R.id.main_frame, fragment)
        transaction.addToBackStack(tag)
        transaction.commit()

        //Se previene doble tap al abrir un fragmento
        Handler(Looper.getMainLooper()).postDelayed({
            this.openingFragment = false
        }, 250)
    }

    fun showAlert(
        message: String,
        listenerNegative: DialogInterface.OnClickListener? = null,
        listenerPositive: DialogInterface.OnClickListener? = null
    ) {

        builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)

        listenerNegative?.let {
            builder?.setNegativeButton(getString(R.string.button_cancel), it)
        }

        builder?.setPositiveButton(getString(R.string.button_accept), listenerPositive)

        val alert = builder?.create()
        alert?.setTitle(getString(R.string.app_name))
        alert?.setMessage(message)

        alert?.show()
    }

    fun showProgress() {

        builder = AlertDialog.Builder(this, R.style.AlertDialogStyle)
        builder?.setCancelable(false)
        builder?.setTitle(getString(R.string.app_name))
        builder?.setMessage(getString(R.string.message_wait_moment))

        dialog = builder?.create()
        dialog?.setCancelable(false)
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.show()
    }

    fun hideProgress() {
        dialog?.dismiss()
    }

    fun updateListBooks(backFragment: Boolean) {

        if (backFragment) {
            onBackPressed()
        }

        val manager = supportFragmentManager

        manager.findFragmentByTag(FragmentListBooks.TAG)?.let {
            if (it is FragmentListBooks) {
                it.updateList()
            }
        }
    }

    override fun onBackPressed() {

        hideKeyboard()

        //Si existen fragmentos en pila se descartan
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()

            configureToolbar(false)

        } else {
            super.onBackPressed()
        }
    }
}