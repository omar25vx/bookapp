package com.example.bookapp.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.bookapp.R
import com.example.bookapp.databinding.LayoutBookDetailBinding
import com.example.bookapp.model.Book
import com.example.bookapp.toJson
import com.example.bookapp.toObject

class FragmentBookDetail: Fragment() {

    private var fragmentBinding: LayoutBookDetailBinding? = null

    private lateinit var book: Book

    private lateinit var textViewTitle: AppCompatTextView
    private lateinit var textViewAuthor: AppCompatTextView
    private lateinit var textViewCategory: AppCompatTextView
    private lateinit var textViewDate: AppCompatTextView
    private lateinit var textViewPages: AppCompatTextView
    private lateinit var textViewIsbn: AppCompatTextView
    private lateinit var textViewPublished: AppCompatTextView
    private lateinit var textViewDescription: AppCompatTextView
    private lateinit var textViewImage: AppCompatImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        if (savedInstanceState != null) {
            getInstance(savedInstanceState)

        } else {
            arguments?.let {
                getInstance(it)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding = LayoutBookDetailBinding.inflate(inflater, container, false)
        fragmentBinding = binding

        textViewTitle = binding.bookDetailTitle
        textViewAuthor = binding.bookDetailAuthor
        textViewCategory = binding.bookDetailCategory
        textViewDate = binding.bookDetailDate
        textViewPages = binding.bookDetailPages
        textViewIsbn = binding.bookDetailIsbn
        textViewPublished = binding.bookDetailPublished
        textViewDescription = binding.bookDetailDescription
        textViewImage = binding.bookDetailImageview

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        textViewTitle.text = book.title ?: "-"
        textViewAuthor.text = book.author?.fullName() ?: "-"
        textViewCategory.text = book.category ?: "-"
        textViewDate.text = book.dateString() ?: "--/--/----"
        textViewPages.text = book.pages ?: "0"
        textViewIsbn.text = book.isbn ?: "-"
        textViewPublished.text = book.published ?: "-"
        textViewDescription.text = book.description ?: "-"

        activity?.let {
            Glide.with(it)
                .load(book.imageURL)
                .placeholder(R.drawable.ic_image_not_supported)
                .into(textViewImage)
        }
    }

    private fun getInstance(savedInstanceState: Bundle) {
        savedInstanceState.getString(BOOK, null)?.let {
            this.book = it.toObject()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(BOOK, this.book.toJson())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        menu.clear()
    }

    override fun onDestroyView() {
        fragmentBinding = null
        super.onDestroyView()
    }

    companion object {
        const val TAG = "FragmentBookDetail"
        const val BOOK = "BOOK"
    }
}