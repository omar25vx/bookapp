package com.example.bookapp.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import com.example.bookapp.R
import com.example.bookapp.databinding.LayoutAddBookBinding
import com.example.bookapp.hideKeyboard
import com.example.bookapp.model.Author
import com.example.bookapp.model.Book
import com.example.bookapp.ui.MainActivity
import com.example.bookapp.util.RetrofiUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class FragmentAddBook: Fragment() {

    private var fragmentBinding: LayoutAddBookBinding? = null

    private lateinit var editTextTitle: AppCompatEditText
    private lateinit var editTextAuthor: AppCompatEditText
    private lateinit var editTextCategory: AppCompatEditText
    private lateinit var editTextPages: AppCompatEditText
    private lateinit var editTextIsbn: AppCompatEditText
    private lateinit var editTextPublished: AppCompatEditText
    private lateinit var editTextDescription: AppCompatEditText
    private lateinit var editTextUrlImagen: AppCompatEditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)

        if (savedInstanceState != null) {
            getInstance(savedInstanceState)

        } else {
            arguments?.let {
                getInstance(it)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding = LayoutAddBookBinding.inflate(inflater, container, false)
        fragmentBinding = binding

        editTextTitle = binding.addBookTitle
        editTextAuthor = binding.addBookAuthor
        editTextCategory = binding.addBookCategory
        editTextPages = binding.addBookPages
        editTextIsbn = binding.addBookIsbn
        editTextPublished = binding.addBookPublished
        editTextDescription = binding.addBookDescription
        editTextUrlImagen = binding.addBookUrlImage

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    private fun getInstance(savedInstanceState: Bundle) {
        editTextTitle.setText(savedInstanceState.getString(TITLE, ""))
        editTextAuthor.setText(savedInstanceState.getString(AUTHOR, ""))
        editTextCategory.setText(savedInstanceState.getString(CATEGORY, ""))
        editTextPages.setText(savedInstanceState.getString(PAGES, ""))
        editTextIsbn.setText(savedInstanceState.getString(ISBN, ""))
        editTextPublished.setText(savedInstanceState.getString(PUBLISHED, ""))
        editTextDescription.setText(savedInstanceState.getString(DESCRIPTION, ""))
        editTextUrlImagen.setText(savedInstanceState.getString(URLIMAGE, ""))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(TITLE, editTextTitle.text.toString())
        outState.putString(AUTHOR, editTextAuthor.text.toString())
        outState.putString(CATEGORY, editTextCategory.text.toString())
        outState.putString(PAGES, editTextPages.text.toString())
        outState.putString(ISBN, editTextIsbn.text.toString())
        outState.putString(PUBLISHED, editTextPublished.text.toString())
        outState.putString(DESCRIPTION, editTextDescription.text.toString())
        outState.putString(URLIMAGE, editTextUrlImagen.text.toString())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        inflater.inflate(R.menu.menu_add_book, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == R.id.menu_add_book_save) {
            validarTextos()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    fun validarTextos() {

        var error = false

        if (editTextTitle.text.toString().isEmpty()) {
            editTextTitle.error = tagError(R.string.title_title)
            error = true
        }
        if (editTextAuthor.text.toString().isEmpty()) {
            editTextAuthor.error = tagError(R.string.title_author)
            error = true
        }
        if (editTextCategory.text.toString().isEmpty()) {
            editTextCategory.error = tagError(R.string.title_category)
            error = true
        }
        if (editTextPages.text.toString().isEmpty()) {
            editTextPages.error = tagError(R.string.title_pages)
            error = true
        }
        if (editTextIsbn.text.toString().isEmpty()) {
            editTextIsbn.error = tagError(R.string.title_isbn)
            error = true
        }
        if (editTextPublished.text.toString().isEmpty()) {
            editTextPublished.error = tagError(R.string.title_published)
            error = true
        }
        if (editTextDescription.text.toString().isEmpty()) {
            editTextDescription.error = tagError(R.string.title_description)
            error = true
        }
        if (editTextUrlImagen.text.toString().isEmpty()) {
            editTextUrlImagen.error = tagError(R.string.title_url_image)
            error = true
        }

        if (error) {
            return

        } else {
            insertBook()
        }
    }

    private fun tagError(string: Int): String {
        return getString(string).toUpperCase(Locale.ROOT).replace(":", "")
    }

    private fun insertBook() {
        activity?.hideKeyboard()

        val activity = activity

        val title = editTextTitle.text.toString()
        val category = editTextCategory.text.toString()
        val pages = editTextPages.text.toString()
        val isbn = editTextIsbn.text.toString()
        val published = editTextPublished.text.toString()
        val description = editTextDescription.text.toString()
        val urlImagen = editTextUrlImagen.text.toString()

        val book = Book()
        book.title = title
        book.author = Author().apply {
            firstName = editTextAuthor.text.toString().substringBefore(" ")
            lastName = editTextAuthor.text.toString().substringAfter(" ")
        }
        book.category = category
        book.createdOn = Date()
        book.pages = pages
        book.isbn = isbn
        book.published = published
        book.description = description
        book.imageURL = urlImagen

        if (activity is MainActivity) {
            activity.showProgress()
            RetrofiUtil.createBook(book, object : Callback<Book> {
                override fun onResponse(call: Call<Book>, response: Response<Book>) {
                    activity.hideProgress()
                    activity.updateListBooks(true)
                }
                override fun onFailure(call: Call<Book>, t: Throwable) {
                    activity.hideProgress()
                    activity.showAlert(t.localizedMessage ?: "Error")
                }
            })
        }
    }

    override fun onDestroyView() {
        fragmentBinding = null
        super.onDestroyView()
    }

    companion object {
        const val TAG = "FragmentBookAdd"

        private const val TITLE = "TITLE"
        private const val AUTHOR = "AUTHOR"
        private const val CATEGORY = "CATEGORY"
        private const val DATE = "DATE"
        private const val PAGES = "PAGES"
        private const val ISBN = "ISBN"
        private const val PUBLISHED = "PUBLISHED"
        private const val DESCRIPTION = "DESCRIPTION"
        private const val URLIMAGE = "URLIMAGE"
    }
}