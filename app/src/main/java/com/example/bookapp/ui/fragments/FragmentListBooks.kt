package com.example.bookapp.ui.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.bookapp.R
import com.example.bookapp.adapters.BooksAdapter
import com.example.bookapp.databinding.LayoutFragmentListBooksBinding
import com.example.bookapp.model.Book
import com.example.bookapp.print
import com.example.bookapp.ui.MainActivity
import com.example.bookapp.util.DeviceUtil
import com.example.bookapp.util.RetrofiUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentListBooks: Fragment(), View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private var fragmentBinding: LayoutFragmentListBooksBinding? = null

    private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var recyclerView: RecyclerView? = null

    private var adapter: BooksAdapter? = null

    private var textFilter = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {

        val binding = LayoutFragmentListBooksBinding.inflate(inflater, container, false)
        fragmentBinding = binding

        swipeRefreshLayout = binding.fragmentListBooksSwiperefresh
        swipeRefreshLayout?.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorPrimary,
            R.color.colorPrimaryDark
        )
        swipeRefreshLayout?.setSize(SwipeRefreshLayout.DEFAULT)
        swipeRefreshLayout?.setOnRefreshListener(this)
        swipeRefreshLayout?.isRefreshing = true

        recyclerView = binding.fragmentListBooksRecyclerview
        recyclerView?.layoutManager = LinearLayoutManager(activity)
        recyclerView?.itemAnimator = DefaultItemAnimator()
        recyclerView?.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))

        setAdapter(mutableListOf())

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        updateList()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()

        inflater.inflate(R.menu.menu_list_books, menu)

        val searchItem = menu.findItem(R.id.menu_list_books_search)
        val searchView = searchItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                textFilter = query ?: ""

                adapter?.filter?.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                textFilter = newText ?: ""

                adapter?.filter?.filter(newText)
                return false
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val activity = activity

        if (item.itemId == R.id.menu_list_books_add) {

            if (!DeviceUtil.hasInternetConnection()) {

                if (activity is MainActivity) {
                    activity.showAlert(getString(R.string.message_without_internet_connection))
                }
                return true
            }

            if (activity is MainActivity) {
                activity.fragmentAddBook()
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {

    }

    override fun onRefresh() {
        updateList()
    }

    fun updateList() {

        if (DeviceUtil.hasInternetConnection()) {

            swipeRefreshLayout?.isRefreshing = true

            RetrofiUtil.getBooks(object : Callback<MutableList<Book>> {
                override fun onResponse(call: Call<MutableList<Book>>, response: Response<MutableList<Book>>) {
                    val books = response.body()

                    books?.toString()?.print()

                    setAdapter(books ?: mutableListOf())

                    swipeRefreshLayout?.isRefreshing = false
                }

                override fun onFailure(call: Call<MutableList<Book>>, t: Throwable) {
                    t.localizedMessage?.print()

                    swipeRefreshLayout?.isRefreshing = false
                }
            })
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                val activity = activity
                if (activity is MainActivity) {
                    activity.showAlert(getString(R.string.message_without_internet_connection))
                }
                swipeRefreshLayout?.isRefreshing = false
            }, 500)
        }
    }

    fun setAdapter(books: MutableList<Book>) {
        activity?.runOnUiThread {
            adapter = BooksAdapter(activity as AppCompatActivity, books)
            adapter?.filter?.filter(textFilter)
            recyclerView?.adapter = adapter
        }
    }

    override fun onDestroyView() {
        fragmentBinding = null
        super.onDestroyView()
    }

    companion object {
        const val TAG = "FragmentListBooks"
    }
}