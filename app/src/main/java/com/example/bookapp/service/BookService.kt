package com.example.bookapp.service

import com.example.bookapp.model.Book
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.http.*


interface BookService {

    @GET("box_479f5c073a80294b4c3b")
    fun getAllBooks(): Call<MutableList<Book>>

    @POST("box_479f5c073a80294b4c3b")
    fun createBook(@Body book: Book?): Call<Book>

    @DELETE("box_479f5c073a80294b4c3b/{RECORD_ID}")
    fun deleteBook(@Path("RECORD_ID") _id: String): Call<JsonElement>


}