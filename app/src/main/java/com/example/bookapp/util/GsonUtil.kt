package com.example.bookapp.util

import com.example.bookapp.log
import com.google.gson.*
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.*

/**
 * Objeto global para serializar y deserializar objetos a Json
 */

@Suppress("unused")
object GsonUtil {

    private var gson: Gson? = null

    fun getGson(): Gson {
        if (gson == null) {
            val gsonBuilder = GsonBuilder()
            gsonBuilder.setLenient()
            gsonBuilder.serializeNulls()

            gson = gsonBuilder.create()
        }
        return gson!!
    }

    /** Validar que sea un objeto json correcto ***************************************************/

    fun isValidJson(json: String): Boolean {
        return try {
            val jsonElement = JsonParser.parseString(json).deepCopy()

            jsonElement != null

        } catch (ignored: Exception) {
            false
        }
    }

    /** Convertir objeto a Json  String ***********************************************************/

    fun <T> toJson(t: T): String {
        return getGson().toJson(t)
    }

    /** Convertir a Objeto Json o Element *********************************************************/

    fun stringToJsonObject(json: String): JsonObject {
        return JsonParser.parseString(json).asJsonObject
    }

    fun stringToJsonElement(json: String): JsonElement {
        return JsonParser.parseString(json)
    }

    fun <T> objectToJsonObject(t: T): JsonObject {
        return getGson().toJsonTree(t).asJsonObject
    }

    fun <T> objectToJsonElement(t: T): JsonElement {
        return getGson().toJsonTree(t)
    }

    /** Deserializar Objeto ***********************************************************************/

    inline fun <reified T> fromJson(o: String): T {
        return getGson().fromJson(o, T::class.java)
    }

    inline fun <reified T> fromJson(o: JsonElement): T {
        return getGson().fromJson(o, T::class.java)
    }

    /** Deserializar Hashmap *********************************************************************/

    inline fun <K, reified V> mapFromJson(o: String): HashMap<K, V> {
        return try {
            getGson().fromJson(o, MapType(V::class.java))

        } catch (e: Exception) {
            e.log()

            hashMapOf()
        }
    }

    inline fun <K, reified V> mapFromJson(o: JsonElement): HashMap<K, V> {
        return try {
            getGson().fromJson(o, MapType(V::class.java))

        } catch (e: Exception) {
            e.log()

            hashMapOf()
        }
    }

    /** Deserializar MutableList ******************************************************************/

    inline fun <reified E> listFromJson(o: String): MutableList<E> {
        return try {
            getGson().fromJson(o, ListType(E::class.java))

        } catch (e: Exception) {
            e.log()

            mutableListOf()
        }
    }

    inline fun <reified E> listFromJson(o: JsonElement): MutableList<E> {
        return try {
            getGson().fromJson(o, ListType(E::class.java))

        } catch (e: Exception) {
            e.log()

            mutableListOf()
        }
    }

    /** LystType **********************************************************************************/

    class ListType<E> constructor(wrapped: Class<E>) : ParameterizedType {

        private val wrapped: Class<*>

        init {
            this.wrapped = wrapped
        }

        override fun getActualTypeArguments(): Array<Type> {
            return arrayOf(wrapped)
        }

        override fun getRawType(): Type {
            return MutableList::class.java
        }

        override fun getOwnerType(): Type? {
            return null
        }
    }

    /** MapType ***********************************************************************************/

    class MapType<V> constructor(wrapped: Class<V>) : ParameterizedType {

        private val wrapped: Class<*>

        init {
            this.wrapped = wrapped
        }

        override fun getActualTypeArguments(): Array<Type> {
            return arrayOf(wrapped)
        }

        override fun getRawType(): Type {
            return HashMap::class.java
        }

        override fun getOwnerType(): Type? {
            return null
        }
    }
}
