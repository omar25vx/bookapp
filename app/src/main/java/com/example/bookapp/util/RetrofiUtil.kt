package com.example.bookapp.util

import com.example.bookapp.model.Book
import com.example.bookapp.service.BookService
import com.google.gson.JsonElement
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofiUtil {

    private const val BASE_URL = "https://jsonbox.io/"

    private fun instanceBooks(): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonUtil.getGson()))
            .build()
    }

    fun getBooks(callback: Callback<MutableList<Book>>) {
        val service = instanceBooks().create(BookService::class.java)
        val call = service.getAllBooks()
        call.enqueue(callback)
    }

    fun createBook(book: Book, callback: Callback<Book>) {
        val service = instanceBooks().create(BookService::class.java)
        val call = service.createBook(book)
        call.enqueue(callback)
    }

    fun deleteBook(_id: String, callback: Callback<JsonElement>) {
        val service = instanceBooks().create(BookService::class.java)
        val call = service.deleteBook(_id)
        call.enqueue(callback)
    }
}