package com.example.bookapp.util

import android.util.Log

object LogUtil {

    fun print(result: Any?) {

        result?.let {

            val objectStr = it.toString()

            Log.v("BookApp: ", objectStr)
        }
    }

    fun exception(e: Exception) {
        e.printStackTrace()
    }
}