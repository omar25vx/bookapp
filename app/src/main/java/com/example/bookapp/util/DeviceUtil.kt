package com.example.bookapp.util

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Surface
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import com.example.bookapp.BookApp

object DeviceUtil {

    val app = BookApp.get()

    private fun connectivityManager(): ConnectivityManager {
        return app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    }

    fun hasInternetConnection(): Boolean {

        val connectivityManager = connectivityManager()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            @Suppress("DEPRECATION")
            return connectivityManager.activeNetworkInfo != null &&
                    connectivityManager.activeNetworkInfo?.isConnected == true
        }
    }

    @Suppress("DEPRECATION")
    fun rotacionPantalla(): Int {
        val wm = app.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            app.display
        } else {
            wm.defaultDisplay
        }
        return display?.rotation ?: 0
    }

    fun lockOrientation(activity: Activity?) {

        activity?.let {

            val rotacionPantalla = rotacionPantalla()

            val orientacionActual = it.resources.configuration.orientation

            if (rotacionPantalla == Surface.ROTATION_0 || rotacionPantalla == Surface.ROTATION_90) {

                if (orientacionActual == Configuration.ORIENTATION_PORTRAIT) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                } else {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                }

            } else if (rotacionPantalla == Surface.ROTATION_180 || rotacionPantalla == Surface.ROTATION_270) {

                if (orientacionActual == Configuration.ORIENTATION_PORTRAIT) {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                } else {
                    activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                }
            }
        }
    }

    fun unLockOrientation(activity: Activity?) {
        activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
    }

    fun hideKeyboard(activity: Activity) {
        try {
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {

        }
    }

    fun showKeyboard(activity: Activity) {
        try {
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        } catch (e: Exception) {

        }
    }
}