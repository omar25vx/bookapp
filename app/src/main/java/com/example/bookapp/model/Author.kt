package com.example.bookapp.model

import com.example.bookapp.toJson
import com.google.gson.annotations.SerializedName

class Author {

    @SerializedName("first_name")
    var firstName: String? = null

    @SerializedName("last_name")
    var lastName: String? = null

    fun fullName(): String {
        return "$firstName $lastName"
    }

    override fun toString(): String {
        return this.toJson()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Author

        if (firstName != other.firstName) return false
        if (lastName != other.lastName) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstName?.hashCode() ?: 0
        result = 31 * result + (lastName?.hashCode() ?: 0)
        return result
    }

}