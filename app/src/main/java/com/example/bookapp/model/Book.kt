package com.example.bookapp.model

import com.example.bookapp.toJson
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

class Book {

    @SerializedName("_id")
    var id: String? = null

    @SerializedName("isbn")
    var isbn: String? = null

    @SerializedName("title")
    var title: String? = null

    @SerializedName("category")
    var category: String? = null

    @SerializedName("pages")
    var pages: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("image_url")
    var imageURL: String? = null

    @SerializedName("author")
    var author: Author? = null

    @SerializedName("_createdOn")
    var createdOn: Date? = null

    @SerializedName("published")
    var published: String? = null

    override fun toString(): String {
        return this.toJson()
    }

    fun search(): String {
        val stringBuilder = StringBuilder()

        id?.let {
            stringBuilder.append(it).append(" ")
        }
        isbn?.let {
            stringBuilder.append(it).append(" ")
        }
        title?.let {
            stringBuilder.append(it).append(" ")
        }
        category?.let {
            stringBuilder.append(it).append(" ")
        }
        pages?.let {
            stringBuilder.append(it).append(" ")
        }
        description?.let {
            stringBuilder.append(it).append(" ")
        }
        author?.let {
            stringBuilder.append(it.firstName ?: "").append(" ")
            stringBuilder.append(it.lastName ?: "").append(" ")
        }
        dateString()?.let {
            stringBuilder.append(it).append(" ")
        }
        published?.let {
            stringBuilder.append(it).append(" ")
        }
        return stringBuilder.toString()
    }

    fun dateString(): String? {
        return if (createdOn != null) {
            val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
            format.format(createdOn!!)
        } else {
            null
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Book

        if (id != other.id) return false
        if (isbn != other.isbn) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (isbn?.hashCode() ?: 0)
        return result
    }


}