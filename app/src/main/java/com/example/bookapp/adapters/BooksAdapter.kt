package com.example.bookapp.adapters

import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.bookapp.R
import com.example.bookapp.databinding.LayoutRowBookBinding
import com.example.bookapp.model.Book
import com.example.bookapp.ui.MainActivity
import com.example.bookapp.util.DeviceUtil
import com.example.bookapp.util.RetrofiUtil
import com.google.gson.JsonElement
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class BooksAdapter constructor(private var activity: AppCompatActivity,
                               private var books: MutableList<Book>) : RecyclerView.Adapter<BooksAdapter.BookViewHolder>(), Filterable {

    private var booksFilter = mutableListOf<Book>()

    init {
        booksFilter.addAll(books)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {

        val binding = LayoutRowBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BookViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book = booksFilter[position]

        holder.title.text = book.title
        holder.author.text = book.author?.fullName() ?: "-"
        holder.category.text = book.category

        Glide.with(activity)
            .load(book.imageURL)
            .placeholder(R.drawable.ic_image_not_supported)
            .into(holder.image)
    }

    override fun getItemCount(): Int {
        return booksFilter.size
    }

    /** */
    inner class BookViewHolder internal constructor(binding: LayoutRowBookBinding) : RecyclerView.ViewHolder(binding.root), View.OnClickListener, View.OnLongClickListener {

        val image: AppCompatImageView = binding.rowBookImageview
        val title: AppCompatTextView = binding.rowBookTitle
        val author: AppCompatTextView = binding.rowBookAuthor
        val category: AppCompatTextView = binding.rowBookCategory

        init {
            binding.root.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            val book = booksFilter[adapterPosition]

            val activity = activity

            if (activity is MainActivity) {
                activity.fragmentBookDetail(book)
            }
        }

        override fun onLongClick(v: View?): Boolean {

            val book = booksFilter[adapterPosition]

            deleteBookDialog(book)

            return true
        }
    }

    private fun deleteBookDialog(book: Book) {
        val activity = activity

        if (activity is MainActivity) {

            if (!DeviceUtil.hasInternetConnection()) {
                activity.showAlert(activity.getString(R.string.message_without_internet_connection))
                return
            }

            val messageStr = activity.getString(R.string.message_delete_book)
            val message = String.format(messageStr, book.title)

            val listenerNegativo = DialogInterface.OnClickListener { _, _ -> }
            val listenerPositivo = DialogInterface.OnClickListener { _, _ ->

                activity.showProgress()

                RetrofiUtil.deleteBook(book.id!!, object : Callback<JsonElement> {
                    override fun onResponse(call: Call<JsonElement>, response: Response<JsonElement>) {
                        activity.hideProgress()
                        activity.updateListBooks(false)
                    }

                    override fun onFailure(call: Call<JsonElement>, t: Throwable) {
                        activity.hideProgress()
                        activity.showAlert(t.localizedMessage ?: "Error")
                    }
                })
            }
            activity.showAlert(message, listenerNegativo, listenerPositivo)
        }
    }

    override fun getFilter(): Filter {
        return filter
    }

    private var filter = object : Filter() {

        override fun performFiltering(constraint: CharSequence?): FilterResults {

            val results = FilterResults()

            var filterList = mutableListOf<Book>()

            if (constraint != null && constraint.isNotEmpty()) {

                val filterString = constraint.toString().toLowerCase(Locale.getDefault())

                filterList = books.filter { filter ->

                    val search = filter.search().toLowerCase(Locale.getDefault())

                    search.contains(filterString)

                }.toMutableList()

            } else {
                filterList.addAll(books)
            }

            results.values = filterList
            results.count = filterList.size

            return results
        }

        @Suppress("UNCHECKED_CAST")
        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {

            results?.let {
                booksFilter = it.values as MutableList<Book>
            }

            notifyDataSetChanged()
        }
    }

}

