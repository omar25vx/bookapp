package com.example.bookapp

import android.app.Activity
import com.example.bookapp.util.DeviceUtil
import com.example.bookapp.util.GsonUtil
import com.example.bookapp.util.LogUtil
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.util.*

/** Genericos ****************************************************************************************/

fun <T> T.toJson(): String {
    return GsonUtil.toJson(this)
}

/** String ****************************************************************************************/

fun String.isValidJson(): Boolean {
    return GsonUtil.isValidJson(this)
}

inline fun <reified T> String.toObject(): T {
    return GsonUtil.fromJson(this)
}

fun String.toJsonObject(): JsonObject {
    return GsonUtil.stringToJsonObject(this)
}

fun String.toJsonElement(): JsonElement {
    return GsonUtil.stringToJsonElement(this)
}

inline fun <reified E> String.toMutableList(): MutableList<E> {
    return GsonUtil.listFromJson(this)
}

inline fun <K, reified V> String.toHashMap(): HashMap<K, V> {
    return GsonUtil.mapFromJson(this)
}

/** JsonElement ****************************************************************************************/

inline fun <reified E> JsonElement.toMutableList(): MutableList<E> {
    return GsonUtil.listFromJson(this)
}

inline fun <reified T> JsonElement.toObject(): T? {
    return GsonUtil.fromJson<T>(this)
}

inline fun <K, reified V> JsonElement.toHashMap(): HashMap<K, V> {
    return GsonUtil.mapFromJson(this)
}

/** Logs ******************************************************************************************/

fun String.print(any: Any?) {
    LogUtil.print(plus(" ").plus(any))
}

fun <T> T.print() {
    LogUtil.print(this)
}

/** Exception *************************************************************************************/

fun Exception.log() {
    LogUtil.exception(this)
}

/** Activity *************************************************************************************/

fun Activity.lockOrientation() {
    DeviceUtil.lockOrientation(this)
}

fun Activity.unLockOrientation() {
    DeviceUtil.unLockOrientation(this)
}

fun Activity.hideKeyboard() {
    DeviceUtil.hideKeyboard(this)
}

fun Activity.showKeyboard() {
    DeviceUtil.showKeyboard(this)
}